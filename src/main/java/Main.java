import java.sql.*;

public class Main {
    public static void main(String[] args) {
        Connection c = null;
        Statement stmt = null;
        Statement stmt2 = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
                            "postgres", "12345");
            c.setAutoCommit(false);
            System.out.println("Opened database successfully");
            stmt = c.createStatement();
            stmt2 = c.createStatement();
            String sql = "SELECT * FROM groups";
            ResultSet resultSet = stmt.executeQuery(sql);
            while (resultSet.next()){
                Group group = new Group();
                group.setGroup_id(resultSet.getInt("group_id"));
                group.setGroup_name(resultSet.getString("group_name"));
                System.out.println(group.toString());
                String sql2 = "SELECT * FROM students WHERE group_id = " + group.getGroup_id();
                ResultSet resultSet2 = stmt2.executeQuery(sql2);
                while (resultSet2.next()){
                    Student student = new Student();
                    student.setId(resultSet2.getInt("student_id"));
                    student.setName(resultSet2.getString("student_name"));
                    student.setPhone(resultSet2.getString("student_phone"));
                    student.setGroup_id(resultSet2.getInt("group_id"));
                    System.out.println(student.toString());
                }
            }
            stmt.close();
            stmt2.close();
            c.commit();
            c.close();

        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
