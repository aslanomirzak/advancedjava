CREATE DATABASE "university";

CREATE TABLE Groups(
	group_id INT,
	group_name VARCHAR(20),
	PRIMARY KEY (group_id)
);

CREATE TABLE Students(
	student_id INT,
	student_name VARCHAR(50),
	student_phone VARCHAR(15),
	group_id INT,
	PRIMARY KEY (student_id)
);