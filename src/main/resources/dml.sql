INSERT INTO groups (group_id, group_name) VALUES (1, 'CS-1902'),(2, 'IT-1905'), (3, 'CS-1904');

INSERT INTO students (student_id, student_name, student_phone, group_id) VALUES
	(1, 'Aslan', '+77474942081', 1), (2, 'Miras', '+77775467898', 2),
	(3, 'Sungat', '+77474955165', 2), (4, 'Ardaktym', '+77779214739', 1),
	(5, 'Ulan', '+77051866598', 3), (6, 'Damir', '+77879785497', 3);